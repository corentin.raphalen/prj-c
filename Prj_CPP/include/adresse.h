#ifndef ADRESSE_H
#define ADRESSE_H
#include <iostream>
#include <sstream>

using namespace std;
class Adresse
{
    public:
        Adresse(string ,string,int,string,string="");
        virtual ~Adresse();

        string Getnumero() const { return numero; }
        void Setnumero(string val) { numero = val; }
        string Getrue() const { return rue; }
        void Setrue(string val) { rue = val; }
        string Getcomplement() const { return complement; }
        void Setcomplement(string val) { complement = val; }
        int Getcodepostal() const { return codepostal; }
        void Setcodepostal(int);
        string Getville() const { return ville; }
        void Setville(string val) { ville = val; }
        string toString() const ;

    protected:

    private:
        string numero;
        string rue;
        string complement;
        int codepostal;
        string ville;
};

#endif // ADRESSE_H
