#ifndef PRIVE_H
#define PRIVE_H
#include "adresse.h"
#include "contact.h"
#include <sstream>

class Prive : public Contact
{
    public:
        Prive(string,string,char,string, Adresse* );
        Prive(string,string,char,string, Adresse* , int);
        virtual ~Prive();

        Adresse* GetAdresseP() const { return adr; }
        void SetAdresseP(Adresse* val){ adr = val; };
        string GetdateN() const { return dateN; }
        void SetdateN(string val) { dateN = val; }
        string toString() const;

    protected:

    private:
        Adresse* adr;
        string dateN;
};

#endif // PRIVE_H
