#ifndef PROFESSIONNEL_H
#define PROFESSIONNEL_H
#include <sstream>
#include "contact.h"
#include "adresse.h"


class Professionnel : public Contact
{
    public:
        Professionnel(string,string,char,string, Adresse* ,string);
        Professionnel(string,string,char,string, Adresse* , string, int);
        virtual ~Professionnel();

        Adresse* GetAdresseP() const { return adr; }
        void SetAdresseP(Adresse* val){ adr = val; };
        string toString() const;
        string Getemail() const { return email; }
        void Setemail(string);
        string GetNomE() const { return NomE; }
        void SetNomE(string);

    protected:

    private:
        Adresse * adr;
        string email;
        string NomE;
};

#endif // PROFESSIONNEL_H
