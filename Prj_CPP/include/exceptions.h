#ifndef EXCEPTIONS_H_INCLUDED
#define EXCEPTIONS_H_INCLUDED
#include <iostream>
#include <stdexcept>

using namespace std;

enum class Erreurs {ERR_NOM, ERR_PRE, ERR_SEXE,ERR_CP, ERR_AUTRES, ERR_NOM_E, ERR_EMAIL};

class Exceptions: exception
{

    private:
        Erreurs codeErreur;
        mutable string message;

    public:
        Exceptions(Erreurs) throw();
        virtual ~Exceptions() throw();

        const char * what() const throw() override;

        string Getmessage() const;


};







#endif // EXCEPTIONS_H_INCLUDED
