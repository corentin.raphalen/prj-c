#ifndef CONTACT_H
#define CONTACT_H
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>
#include <algorithm>

using namespace std;

class Contact
{
    public:
        /** Default constructor */
        Contact(string,string,char);
        /**Alternative constructor */
        Contact(string,string,char,int);

        virtual ~Contact();

        int GetID() const { return ID; }
        void SetID(int val) { ID = val; }
        void SetNom(string);
        string GetNom() const { return Nom; }
        void SetPrenom(string);
        string GetPrenom() const { return Prenom; }
        char GetSexe() const { return Sexe; }
        void SetSexe(char);
        virtual string toString() const=0 ;


    protected:

    private:
        int ID;
        string Nom;
        string Prenom;
        char Sexe;
};

#endif // CONTACT_H
