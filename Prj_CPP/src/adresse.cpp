#include "adresse.h"
#include "exceptions.h"
#include <algorithm>
Adresse::Adresse(string n,string r,int cp,string v,string comp)
{
    Setnumero(n);
    Setrue(r);
    Setcodepostal(cp);
    transform(v.begin(), v.end(),v.begin(), ::toupper);
    Setville(v);
    Setcomplement(comp);
}
void Adresse::Setcodepostal(int val)
{
    if(to_string(val).length() == 5 ) codepostal = val;
    else throw Exceptions(Erreurs::ERR_CP);
}

string Adresse::toString() const
{
    ostringstream oss;

    oss <<"         "<< this->Getnumero() <<", "<< this->Getrue() << endl
    <<"         "<<this->Getcodepostal()<<" "<< this->Getville() <<endl;

    return oss.str();
}

Adresse::~Adresse()
{
    cout <<"destruction adresse"<<endl;
}
