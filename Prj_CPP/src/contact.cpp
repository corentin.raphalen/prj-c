#include "contact.h"
#include <string>
#include <iostream>
#include <cstring>
#include <cctype>
#include <algorithm>
#include "exceptions.h"

using namespace std;

Contact::Contact(string nom, string prenom, char sexe)
{
    SetNom(nom);
    SetPrenom(prenom);
    SetSexe(sexe);
}

Contact::Contact(string nNom, string nPrenom, char nSexe, int nID )
{
    SetID(nID);
    SetNom(nNom);
    SetPrenom(nPrenom);
    SetSexe(nSexe);
}
void Contact::SetPrenom(string val)
{
    if (val.length() <= 30)
    {
        transform(begin(val), end(val),begin(val), ::tolower);
        val[0] = toupper(val[0]);
        Prenom=val;
    }
    else throw Exceptions(Erreurs::ERR_PRE);
}
void Contact::SetNom(string val)
{
    if (val.length() <= 30)
    {
        transform(val.begin(), val.end(),val.begin(), ::toupper);
        Nom=val;
    }
    else throw Exceptions(Erreurs::ERR_NOM);
}
void Contact::SetSexe(char val)
{
    if (val=='M' || val=='F')
    {
        Sexe = val;
    }
    else throw Exceptions(Erreurs::ERR_SEXE);
}

Contact::~Contact()
{
    cout<<"destruction Contact"<<endl;
}
