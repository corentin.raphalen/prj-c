#include "priv�.h"
#include <ctime>
#include <chrono>

string getTimeStr(){
    time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    string s(30, '\0');
    std::strftime(&s[0], s.size(), "%d/%m", std::localtime(&now));
    return s;
    }

string age(string data)
{
    int day, month, year;
    struct tm date = {0};
    day= stoi(data.substr(0,2));
    month=stoi(data.substr(4,2));
    year=stoi(data.substr(6,4));
    date.tm_year = year-1900;
    date.tm_mon  = month-1;
    date.tm_mday = day;
    time_t normal = mktime(&date);
    time_t current;
    time(&current);
    int age = (difftime(current, normal) + 86400L/2) / 86400L;
    age=age/365;
    string message;
    if (getTimeStr().substr(0,5) == data.substr(0,5))
    {
        message="Age : "+to_string(age)+" ans "+"Et Bon Anniversaire !!!";
        return message;
    }
    else
    {
        message="Age : "+to_string(age)+" ans ";
        return message;
    }
}
Prive::Prive(string n,string p,char s,string dn, Adresse* ad)
    :Contact(n,p,s)
{
    SetdateN(dn);
    this->adr = ad;
}
Prive::Prive(string n,string p,char s,string dn, Adresse* ad, int id)
    :Contact(n,p,s,id)
{
    SetdateN(dn);
    this->adr = ad;
}
string Prive::toString() const
{
    ostringstream oss;

    oss << "Particulier : " << this->GetID() << endl
    <<"         ";
    if (this->GetSexe() =='F'){oss << "Mme. " ;}
    else {oss<<"M. ";}
    oss << this->GetNom()<<" "<<this->GetPrenom()<<endl;
    oss << this->adr->toString() ;
    oss <<"         "<< age(this->GetdateN()) <<endl;

    return oss.str();
}

Prive::~Prive()
{
    cout <<"Destruction Prive"<<endl;
}
