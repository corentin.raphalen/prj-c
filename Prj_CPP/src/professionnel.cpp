#include "professionnel.h"
#include "exceptions.h"

Professionnel::Professionnel(string n,string p,char s,string dn, Adresse* ad, string em)
    :Contact(n,p,s)
{
    SetNomE(dn);
    Setemail(em);
    this->adr = ad;
}

Professionnel::Professionnel(string n,string p,char s,string dn, Adresse* ad, string em, int id)
    :Contact(n,p,s,id)
{
    SetNomE(dn);
    Setemail(em);
    this->adr = ad;
}

void Professionnel::SetNomE(string val)
{
    if (val.length() <= 50)
    {
        transform(val.begin(), val.end(),val.begin(), ::toupper);
        NomE=val;
    }
    else throw Exceptions(Erreurs::ERR_NOM_E);
}

void Professionnel::Setemail(string val)
{
    if(val.find("@") != string::npos)
    {
        email = val;
    }
    else throw Exceptions(Erreurs::ERR_EMAIL);
}

string Professionnel::toString() const
{
    ostringstream oss;

    oss << "Professionnel : " << this->GetID() << endl
    <<"         Societ� : "<<this->GetNomE()<<endl
    <<"         Contact : ";

    if (this->GetSexe() =='F'){oss << "Mme " ;}
    else {oss<<"Mr ";}

    oss<< this->GetNom()<<" "<<this->GetPrenom()<<endl;
    oss << this->adr->toString()<<endl ;
    oss <<"         "<< "Mail : "<< this->Getemail() <<endl;

    return oss.str();
}
Professionnel::~Professionnel()
{
    cout << "Destruction Professionnel"<<endl;
}
