#include "Exceptions.h"



Exceptions::Exceptions(Erreurs e) throw()
{
    this->codeErreur = e;
}

Exceptions::~Exceptions() throw()
{
    cout << "Destruction Erreur " << endl;
}

const char* Exceptions::what() const throw()
{
    this->Getmessage();
    return message.c_str();
}

string Exceptions::Getmessage() const
{
    switch (this->codeErreur)
    {
        case Erreurs::ERR_NOM:
            message = "Nom doit faire 30 char maximum !!!";
            break;
        case Erreurs::ERR_NOM_E:
            message = "Nom Entreprise doit faire 50 char maximum !!!";
            break;
        case Erreurs::ERR_PRE:
            message = "Prenom doit faire 30 char maximum !!!";
            break;
        case Erreurs::ERR_SEXE:
            message = "Sexe doit �tre M ou F uniquement !!!";
            break;
        case Erreurs::ERR_CP:
            message = "CP doit comporter 5 chiffres !!!";
            break;
        case Erreurs::ERR_EMAIL:
            message = "Email doit comporter un @ !!!";
            break;
        default:
            message = "Autre Erreur !";
            break;
    }
    return this->message;
}

