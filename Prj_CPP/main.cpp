#include <iostream>
#include <stdio.h>
#include "./sqlite-amalgamation-3270200/sqlite3.h"
#include "exceptions.h"
#include "priv�.h"
#include "adresse.h"
#include "professionnel.h"
#include <fstream>
#include <ctime>
#include <chrono>
#include <map>
#include <vector>

using namespace std;


/*
static int callback(void* data, int argc, char** argv, char** azColName)
{
    int i;
    //fprintf(stderr, "%s: ", (const char*)data);
    fprintf(stderr, "=====================\n");

    for (i = 0; i < argc; i++) {
        printf("%s : %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }

    printf("\n");
    return 0;
}
*/
ostream& operator <<(ostream& sortie, const Prive& objPrive )
{
    sortie << objPrive.toString();
    return sortie;
}

ostream& operator <<(ostream& sortie, const Professionnel& objProf )
{
    sortie << objProf.toString();
    return sortie;
}

int main()
{
     //MENU
    string tempNom, tempPrenom, tempNumero, tempNomEntr, tempRue, tempComplement,tempVille, tempMail, tempDateN,tempS,tempCP;
    int tempCodePostal ;
    char tempSexe;
    Adresse* tempAdr;
    Prive* newContactPriv;
    Professionnel* newContactPro;
    string choixMenu3;


    cout << "Quelle action souhaitez vous faire ?" << endl;
    cout << "1: Lister les contacts." << endl
    << "2: Recherche a partir de criteres." << endl
    << "3: Ajouter un contact a la base de donnee." << endl
    << "4: Supprimer un contact" << endl;

    string choixMenuPrincipal;
    getline(cin,choixMenuPrincipal);

    switch (stoi(choixMenuPrincipal))
    {
        case 1:
            cout << "Choix 1";
    /*
    sqlite3 *db;        // connection Base
    int rc;             // code retour
    char *errmsg;       // pointeur vers err



    sqlite3_stmt *stmt;
    int nbCols=0;

    rc = sqlite3_open("dbContacts.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 0;
    }
    printf("Base ouverte avec succ�s.\n");


    map<int,Contact* > mapPri;
    map<int,Contact* > mapPro;
    sqlite3_prepare_v2(db,
                       "select * from contacts",
                       -1,
                       &stmt, 0);

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        try
        {
            Contact* c =nullptr;


            int Id= sqlite3_column_int(stmt, 0);
            string Nom=string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 1)));
            string Prenom=string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 2)));
            string truc= string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 3)));
            char Sexe=truc[0];
            truc=string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 5)));
            truc=truc[0];
            string Rue=string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 5)));
            int Cp=sqlite3_column_int(stmt, 7);
            string Ville=string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 8)));
            string Complement;
            if (sqlite3_column_text(stmt, 6) == NULL)
            {
                Complement="";
            }
            else Complement=string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 6)));
            cout<<"CP : "<<Cp<<endl;
            Adresse* a=new Adresse(truc,Rue,Cp,Ville,Complement);

            if (sqlite3_column_text(stmt, 4) ==NULL)
            {
                string DateN=string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 10)));
                c = new Prive(Nom, Prenom, Sexe, DateN, a, Id);
                mapPri[c->GetID()]=c;
            }
            else
            {
                string NomE=string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 4)));
                string Email=string(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 9)));
                c = new Professionnel(Nom,Prenom,Sexe,NomE,a,Email,Id);
                mapPro[c->GetID()]=c;
            }
        }
        catch(const Exceptions& ex)
        {
            cout << ex.what() << endl;
        }
            cout<< mapPro[0]<<endl;
    }







    sqlite3_finalize(stmt);

    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Close : %s\n", sqlite3_errmsg(db));
       return 0;
    }
*/
            break;

        case 2:
            cout << "Choix 2";
            break;
        case 3:
            cout << "Souhaitez-vous ajouter un contact prive ou professionnel ?" << endl;
            cout << "1: Ajouter un contact prive." << endl
            << "2: Ajouter un contact porfessionnel." << endl;


            getline(cin,choixMenu3);

                switch(stoi(choixMenu3))
                {
                case 1:
                    //NE GERE PAS LES ESPACES -> TERMINE LA LECTURE DE LA CHAINE
                    cout << "Saisir le Nom du contact : ";
                    getline(cin,tempNom);
                    cout << "Saisir le Prenom du contact : ";
                    getline(cin,tempPrenom);
                    cout << "Saisir le Sexe du contact : ";
                    getline(cin,tempS);
                    tempSexe=tempS[0];
                    cout << "Saisir la Rue du contact : ";
                    getline(cin,tempRue);
                    cout << "Saisir le numero de Rue du contact : ";
                    getline(cin,tempNumero);
                    cout << "Saisir le Complement du contact (laissez vide en s'il n'y a pas de complement) : ";
                    getline(cin ,tempComplement);
                    cout << "Saisir le Code Postal du contact : ";
                    getline(cin,tempCP);
                    tempCodePostal=stoi(tempCP);
                    cout << "Saisir la Ville du contact : ";
                    getline(cin,tempVille);
                    cout << "Saisir la date de naissance du contact (format jj/mm/aaaa): ";
                    getline(cin,tempDateN);

                    tempAdr = new Adresse(tempNumero, tempRue, tempCodePostal, tempVille, tempComplement);
                    newContactPriv = new Prive(tempNom, tempPrenom, tempSexe, tempDateN, tempAdr);
                    break;

                case 2:
                    cout << "Saisir le Nom du contact : ";
                    getline(cin,tempNom);
                    cout << "Saisir le Prenom du contact : ";
                    getline(cin,tempPrenom);
                    cout << "Saisir le Sexe du contact : ";
                    getline(cin,tempS);
                    tempSexe=tempS[0];
                    cout << "Saisir le nom de l'entreprise du contact : ";
                    getline(cin,tempNomEntr);
                    cout << "Saisir la Rue du contact : ";
                    getline(cin,tempRue);
                    cout << "Saisir le numero de Rue du contact : ";
                    getline(cin,tempNumero);
                    cout << "Saisir le Complement du contact (laissez vide en s'il n'y a pas de complement) : ";
                    getline(cin ,tempComplement);
                    cout << "Saisir le Code Postal du contact : ";
                    getline(cin,tempCP);
                    tempCodePostal=stoi(tempCP);
                    cout << "Saisir la Ville du contact : ";
                    getline(cin,tempVille);
                    cout << "Saisir le mail du contact : ";
                    getline(cin,tempMail);

                    tempAdr = new Adresse(tempNumero, tempRue, tempCodePostal, tempVille, tempComplement);
                    newContactPro = new Professionnel(tempNom, tempPrenom, tempSexe, tempNomEntr, tempAdr, tempMail);
                    break;

                default:
                    cout << "Input error." << endl;
                    break;
                }

            break;
        case 4:
            cout << "Saisissez l'ID de l'�l�ment � supprimer." << endl;
            break;
        default:
            cout << "Input Error" << endl;
            break;
    }
    cout<< *newContactPriv<<endl;
    delete newContactPriv;
    delete newContactPro;
    /*
    cout << "Cas 1 : " ;
    try
    {
        Contact p = Contact("Dupont","marCel",'M');
        cout<< p.GetNom() << " " << p.GetPrenom()<< " "<< p.GetSexe() <<endl;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }
    cout << "Cas 2 : ";
    try
    {
        Contact p = Contact("Dupontmarioulouloulouloulouloumariouloulouloulouloulou","marCel",'M');
        cout<< p.GetNom() << " " << p.GetPrenom()<< " "<< p.GetSexe() <<endl;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }
    cout << "Cas 3 : ";
    try
    {
        Contact p = Contact("Dupont","marCelmarioulouloulouloulouloumariouloulouloulouloulou",'M');
        cout<< p.GetNom() << " " << p.GetPrenom()<< " "<< p.GetSexe() <<endl;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }
    cout << "Cas 4 : ";
    try
    {
        Contact p = Contact("Dupont","marCel",'G');
        cout<< p.GetNom() << " " << p.GetPrenom()<< " "<< p.GetSexe() <<endl;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }
    cout <<"Cas 5 : ";
    try
    {
        Adresse* ad= new Adresse("3","rue du puy","352500","Anduil","proche parking");
        Prive J("DUrant","Jean-Claude",'M',"08/06/1990",ad);
        cout << J <<endl;
        delete ad;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }

    cout <<"Cas 6 : ";

    try
    {
        Adresse* ad= new Adresse("3","rue du puy","3525","Anduil","proche parking");
        Prive J("DUrant","Jean-Claude",'M',"08/06/1990",ad);
        cout <<J.GetAdresseP()->Getcodepostal() <<endl;
        delete ad;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }

    cout <<"Cas 7 : "<<endl;

    try
    {
        Adresse* ad= new Adresse("3","rue du puy","35250","Anduil","proche parking");
        Prive J("DUrant","Jean-Claude",'M',"14/06/1990",ad);
        cout << J <<endl;
        delete ad;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }


     cout <<"Cas 8 : ";
    try
    {
        Adresse* ad= new Adresse("3","rue du puy","35250","Anduil","proche parking");
        Professionnel J("DUrant","Jean-Claude",'M',"Dicotoqdzdsqsdqsdqsdqzqdsqdsqsdqsdsqdsdsqsdsqsdqsdqsdq",ad,"jctruc.fr");
        cout << J <<endl;
        delete ad;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }

    cout <<"Cas 9 : ";

    try
    {
        Adresse* ad= new Adresse("3","rue du puy","35250","Anduil","proche parking");
        Professionnel J("DUrant","Jean-Claude",'M',"Dicoto",ad,"jctruc.fr");
        cout <<J.GetAdresseP()->Getcodepostal() <<endl;
        delete ad;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }

    cout <<"Cas 10 : "<<endl;

    try
    {
        Adresse* ad= new Adresse("3","rue du puy","35250","Anduil","proche parking");
        Professionnel J("DUrant","Jean-Claude",'M',"Dicoto",ad,"jc@truc.fr");
        cout << J <<endl;
        delete ad;
    }
    catch(const Exceptions& ex)
    {
        cout << ex.what() << endl;
    }
    */





    return 0;
}
